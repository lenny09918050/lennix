let
  e = device: (import <nixpkgs/nixos> {
    configuration = import ./_configuration.nix { device = device; };
  }).system;

  machines = {
    lombard = e ./devices/lombard;
    portable = e ./devices/portable;
  };
in
{
  inherit machines;

  all = [ machines.lombard machines.portable ];
}

