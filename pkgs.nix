{ config, pkgs, lib, ...}:

with pkgs;
{
    # more common config

    services.netdata.enable = true;

    environment.systemPackages = [
        lxterminal
        pcmanfm

        anydesk

        chromium
        firefox-devedition-bin
        vlc
        # inkscape
        # gimp
        nodejs-12_x
        yarn
        tdesktop
        # android-studio
        # nitrux-icon-theme
        # dropbox
        vscode

        git
    
        # htop: process monitor
        htop
        # nload/nethogs: network monitor
        nload nethogs
        # iotop: disk monitor
        iotop
        # wget/curl: HTTP
        wget curl
        # nano: editor
        nano
        # tree/ncdu: file analytics
        tree ncdu
        # glances: netdata for cli, basically
        python38Packages.glances

        clipit
    ];
}
