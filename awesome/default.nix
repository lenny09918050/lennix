{ config, pkgs, lib, ...}:

with pkgs;
let
    lenny-theme = stdenv.mkDerivation {
        pname = "lenny-theme";
        version = "0.0.1";

        src = ./lenny-theme;

        postPatch = ''
            sed "s|@THEME@|$out/share/awesome/lenny-theme|g" -i theme.lua
            '';

        installPhase = ''
            mkdir -p $out/share/awesome
            cp -r $PWD $out/share/awesome/lenny-theme
            '';
    };

    lenny-awesome-config = stdenv.mkDerivation {
        pname = "lenny-awesome-config";
        version  ="0.0.1";

        src  = ./lenny-awesome-config;

        buildInputs = [
            lenny-theme
        ];

        postPatch = ''
            sed "s|@THEME@|${lenny-theme}/share/awesome/lenny-theme|g" -i rc.lua
            '';

        installPhase = ''
            install -D $PWD/rc.lua $out/etc/xdg/awesome/rc.lua
            '';
    };
in
{
    services.xserver.windowManager.awesome = {
        enable = true;
    };

    #etc.xdg.awesome."rc.lua" = {
    #    text = "";
    #};

    environment.systemPackages = [
        lenny-awesome-config
        lenny-theme
    ];
}
