{ device }:
{ config, pkgs, lib, ...}:

with pkgs;
{
    # more common config

    imports = [
        device
        ./awesome
        ./pkgs.nix
    ];

    users.users.lenny = {
        createHome = true;
        isNormalUser = true;
        extraGroups = [ "wheel" "networkmanager" "audio" "video" ];
    };

    services.openssh = {
        enable = true;
    };

    networking.networkmanager.enable = true;

    programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

    # Select internationalisation properties.
    i18n = {
        consoleFont = "Lat2-Terminus16";
        consoleKeyMap = "de";
        defaultLocale = "de_DE.UTF-8";
    };

    # Set your time zone.
    time.timeZone = "Europe/Berlin";

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.extraConfig = ''
    load-module module-equalizer-sink
    load-module module-dbus-protocol
  '';

  # Enable touchpad support.
  services.xserver.libinput.enable = true;

  # Enable powerManagment
  powerManagement.enable = true;
  services.upower.enable = true;
  services.acpid.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

    services.xserver = {
        enable = true;

        displayManager.lightdm.greeters.mini = {
            enable = true;
            user = "lenny";
            extraConfig = ''
            [greeter]
            show-password-label = false
            [greeter-theme]
            background-image = ""
            '';
        };

        windowManager = {
            default = "awesome";
        };

        layout = "de";
        xkbVariant = "nodeadkeys";
        xkbModel = "pc105";
        xkbOptions = "caps:super";
    };

    system.stateVersion = "20.03";

    # Put stallman on fire?
    nixpkgs.config.allowUnfree = true;
}
