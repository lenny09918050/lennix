{ config, pkgs, lib, ...}:

with pkgs;
{
    # more common config

    imports = [
        ./hardware-configuration.nix
    ];

    networking.hostName = "lombard";
}
